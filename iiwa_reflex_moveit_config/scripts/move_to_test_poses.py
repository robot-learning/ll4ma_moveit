#!/usr/bin/env python
from __future__ import print_function

import rospy
import argparse

from moveit_interface import IiwaPlanner

    
if __name__ == '__main__':
    rospy.init_node('iiwa_planner', anonymous=True)
    
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--pose', type=str, required=True,
                        choices=['zero', 'home'] \
                        + ['test_pose_{}'.format(i) for i in range(1,6)] \
                        + ['j{}p'.format(i) for i in range(1,8)] \
                        + ['j{}m'.format(i) for i in range(1,8)])
    parser.add_argument('-v', '--vel_scaling', type=float, default=0.1)
    parser.add_argument('-a', '--acc_scaling', type=float, default=0.1)
    parser.add_argument('-r', '--rate', type=float, default=100)
    parser.add_argument('-n', '--robot_name', type=str, default='iiwa')
    parser.add_argument('-w', '--wait_after_commanded', type=float, default=0)
    parser.add_argument('--plot', action='store_true')
    parser.add_argument('--no_plot', dest='plot', action='store_false')
    parser.set_defaults(plot=False)
    args = parser.parse_args()

    if args.vel_scaling <= 0. or args.vel_scaling > 1.:
        parser.error("Velocity scaling must be in range (0,1]")
    if args.acc_scaling <= 0. or args.acc_scaling > 1.:
        parser.error("Acceleration scaling must be in range (0,1]")

    planner = IiwaPlanner(args.rate, args.robot_name)
    planner.wait_for_joint_state()
    traj = planner.get_plan(args.pose, {}, args.vel_scaling, args.acc_scaling)
    planner.command_trajectory(traj, args.plot, False,
                               wait_secs_after_commanded=args.wait_after_commanded)
