import trimesh
from shape_msgs.msg import Mesh, MeshTriangle
from geometry_msgs.msg import Point

def load_mesh(path):
    mesh = trimesh.load(path, force='mesh')
    #mesh = trimesh.load_collada(path)
    ros_mesh = Mesh()
    for vertex in mesh.vertices:
        point = Point()
        point.x = vertex[0]
        point.y = vertex[1]
        point.z = vertex[2]
        ros_mesh.vertices.append(point)
    for face in mesh.faces:
        mesh_triangle = MeshTriangle()
        mesh_triangle.vertex_indices = face
        ros_mesh.triangles.append(mesh_triangle)
    return ros_mesh

def main():
    load_mesh("/home/mmatak/result.obj")

if __name__ == "__main__":
    main()


