import os
import sys
import rospy
import numpy as np
import roslib.packages as rp
import rospy

from moveit_msgs.msg import PlanningScene, CollisionObject, ObjectColor
from shape_msgs.msg import SolidPrimitive, Mesh, MeshTriangle
from geometry_msgs.msg import Point, Pose, PoseStamped

from moveit_interface.srv import GetPlan, GetPlanRequest
from urdf_parser_py.urdf import URDF

sys.path.append(rp.get_pkg_dir("grasp_pipeline") + "/src/grasp_client/")
from utils import call_service

sys.path.append(rp.get_pkg_dir("moveit_interface") + "/src/moveit_interface/")
from mesh_loading import load_mesh



GET_PLAN_SERVICE_NAME = "/moveit_interface_service/get_plan"
UPDATE_ENV_SERVICE_NAME = "/moveit_interface_service/update_env"
DEFAULT_ASSET_ROOT = os.path.dirname(rp.get_pkg_dir("ll4ma_robots_description"))


def get_plan(target_pose, target_configuration, start_joint_state, end_effector_frame,
             objects={}, max_vel_factor=0.3, max_acc_factor=0.1, max_plan_attempts=3,
             planning_time=5., cartesian_path=False, min_cartesian_pct=1.):
    """
    Helper function to encapsulate making a service request to get a motion plan.
    """
    resp = None
    success = False
    plan_attempts = 0
    
    assert target_pose is None or target_configuration is None

    while resp is None and plan_attempts < max_plan_attempts:
        req = GetPlanRequest()
        req.end_effector_frame = end_effector_frame

        if target_pose is not None:
            if isinstance(target_pose, str):
                req.named_target = target_pose
            elif isinstance(target_pose, PoseStamped):
                req.target_pose = target_pose
            else:
                raise ValueError(f"Unknown type for target pose: {type(target_pose)}")
            
            rospy.loginfo("get_plan triggered with target pose as the goal")
            req.go_to_pose = True
            req.target_pose = PoseStamped()
            req.target_pose.header = target_pose.header
            req.target_pose.pose = Pose()
            req.target_pose.pose.position = target_pose.pose.position
            req.target_pose.pose.orientation = target_pose.pose.orientation
            print("req.target_pose: ")
            print(req.target_pose)
        elif target_configuration is not None:
            rospy.loginfo("get_plan triggered with target config as the goal")
            req.go_to_configuration = True
            req.target_configuration = target_configuration

        req.max_vel_factor = max_vel_factor
        req.max_acc_factor = max_acc_factor
        req.cartesian_path = cartesian_path
        req.planning_time = planning_time
        req.planning_scene = get_planning_scene(start_joint_state, objects)
        rospy.loginfo("planning scene created")
        resp = call_service(GET_PLAN_SERVICE_NAME, req, GetPlan)
        if resp is None or not resp.success or \
           (cartesian_path and resp.cartesian_path_pct < min_cartesian_pct):
            rospy.loginfo("Couldn't generate a plan, trying again")
            resp = None
            success = False
            plan_attempts += 1
        else:
            break
    return resp

def update_environment(objects):
    req = GetPlanRequest()
    req.planning_scene = get_planning_scene(None, objects)
    rospy.loginfo("planning scene created")
    resp = call_service(UPDATE_ENV_SERVICE_NAME, req, GetPlan)
    return resp

def get_planning_scene(joint_state=None, objects={}):
    """
    Creates a PlanningScene instance for collision checking in MoveIt.

    Args:
        joint_state (JointState): Robot joint state (optional)
        objects (dict): Dictionary with object name keys and object configuration values.
                        Should specify everything needed to create the collision objects
                        for each object.
    """
    planning_scene = PlanningScene()
    
    if joint_state:
        planning_scene.robot_state.joint_state = joint_state

    for obj_name, obj_config in objects.items():
        collision_obj = CollisionObject()
        collision_obj.header.frame_id = obj_config['frame_id']
        collision_obj.id = obj_name
        collision_obj.operation = obj_config['operation']
        if obj_config['object_type'] == 'box':
            prim = get_box_prim(obj_config['extents'])
            collision_obj.primitives.append(prim)
            collision_obj.primitive_poses.append(obj_config['pose'])
        elif obj_config['object_type'] == 'mesh':
            mesh = load_mesh(obj_config['path'])
            collision_obj.meshes.append(mesh)
            collision_obj.mesh_poses.append(obj_config['pose'])
        else:
            raise ValueError("Unknown object type for collision object")

        planning_scene.world.collision_objects.append(collision_obj)
        
        if 'color' in obj_config:
            obj_color = ObjectColor()
            obj_color.id = obj_name
            obj_color.color = obj_config['color']
            planning_scene.object_colors.append(obj_color)

    return planning_scene


def get_box_prim(dims):
    """
    Constructs a SolidPrimitive of type BOX.
    """
    prim = SolidPrimitive()
    prim.type = prim.BOX
    prim.dimensions = dims
    return prim


def get_mesh(verts, faces):
    prim = Mesh()
    for v in verts:
        prim.vertices.append(Point(*v))
    for f in faces:
        prim.triangles.append(MeshTriangle(f.tolist()))
    return prim


if __name__ == '__main__':
    from geometry_msgs.msg import Pose
    '''
    asset_filename = os.path.join(ros_util.get_path("ll4ma_robots_description"), "urdf",
                                  "environment", "static", "basket.urdf")
    pose = Pose()
    pose.orientation.w = 1
    print(urdf_to_collision(asset_filename, pose))
    '''
