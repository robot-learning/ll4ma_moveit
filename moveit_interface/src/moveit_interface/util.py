import os
import sys
import rospy
import numpy as np
from urdfpy import URDF
from copy import deepcopy

from moveit_msgs.msg import PlanningScene, CollisionObject, AttachedCollisionObject, ObjectColor, \
                            PlanningSceneComponents 
from moveit_msgs.srv import ApplyPlanningScene, GetPlanningScene
from shape_msgs.msg import SolidPrimitive, Mesh, MeshTriangle
from geometry_msgs.msg import Point, Pose, PoseStamped
from sensor_msgs.msg import JointState

from moveit_interface.srv import (
    GetPlan, GetPlanRequest,
    GetFK, GetFKRequest,
    GetJacobian, GetJacobianRequest,
    GetIK, GetIKRequest,
    GetJointInfo, GetJointInfoRequest,
    GetJacobianPInvPlan, GetJacobianPInvPlanRequest
)

from ll4ma_util import file_util, ros_util

NAMESPACE = "/moveit_interface_service"
GET_PLAN_SRV = f"{NAMESPACE}/get_plan"
GET_JAC_PINV_PLAN_SRV = f"{NAMESPACE}/get_jacobian_pinv_plan"
GET_FK_SRV = f"{NAMESPACE}/get_fk"
GET_IK_SRV = f"{NAMESPACE}/get_ik"
GET_JAC_SRV = f"{NAMESPACE}/get_jacobian"
GET_JOINT_INFO_SRV = f"{NAMESPACE}/get_joint_info"
APPLY_PLANNING_SCENE_SRV = f"/apply_planning_scene"
GET_PLANNING_SCENE_SRV = f"/get_planning_scene"
DEFAULT_ASSET_ROOT = os.path.dirname(ros_util.get_path("ll4ma_robots_description"))


def get_plan(
        target,
        joint_state,
        end_effector_frame,
        objects={},
        planning_scene=None,
        max_vel_factor=0.3,
        max_acc_factor=0.1,
        max_plan_attempts=1,
        planning_time=1.,
        cartesian_path=False,
        min_cartesian_pct=1.,
        target_configuration=False,
        planner_id=None,
        constraints=None,
        level_constraint=False,
):
    """
    Helper function to encapsulate making a service request to get a motion plan.
    target: (one of geometry_msgs.msg.PoseStamped, sensor_msgs.msg.JointState) target to plan to
    joint_state: (list) (7,) current joint positions of the robot
    end_effector_frame:
    objects: (dict)
    planning_scene: (moveit_msgs/PlanningScene) the scene to plan in [default is None]
    max_vel_factor: (float) [default=0.3]
    max_acc_factor: (float) [default=0.1]
    max_plan_attempts: (int) [default=1]
    planning_time: (float) time in seconds [default=1.]
    cartesian_path: (bool) [default=False]
    min_cartesian_pct: (float) minimum number of cartesian points for a valid trajectory [default=1.]
    target_configuration: (bool) whether to interpret the target as a joint configuration [default=False]
    """
    resp = None
    success = False
    plan_attempts = 0
    while resp is None and plan_attempts < max_plan_attempts:
        req = GetPlanRequest()
        req.end_effector_frame = end_effector_frame
        if target_configuration:
            assert len(target) == len(joint_state.position)
            req.go_to_pose = False
            req.go_to_configuration = True
            req.target_configuration = target
        else:
            req.go_to_pose = True
            if isinstance(target, str):
                req.named_target = target
            elif isinstance(target, PoseStamped):
                req.target_pose = target
            else:
                raise ValueError(f"Unknown type for target pose: {type(target_pose)}")
        req.max_vel_factor = max_vel_factor
        req.max_acc_factor = max_acc_factor
        req.cartesian_path = cartesian_path
        req.planning_time = planning_time
        if planning_scene is None:
            planning_scene = get_planning_scene(objects=objects)
        planning_scene.robot_state.joint_state = joint_state
        req.planning_scene = planning_scene
        if planner_id is not None:
            req.plannerId = planner_id
        # if constraints is not None:
        #     req.constraints = constraints
        req.level_constraint = level_constraint

        resp, success = ros_util.call_service(req, GET_PLAN_SRV, GetPlan)
        if resp is None or not success or \
           (cartesian_path and resp.cartesian_path_pct < min_cartesian_pct):
            # Couldn't generate a plan, try again
            resp = None
            success = False
            plan_attempts += 1
        else:
            if target_configuration:
                joint_dist = ((np.array(resp.trajectory.joint_trajectory.points[-1].positions) - np.array(target))**2).sum()
                if joint_dist > 1e-2:
                    resp = None
                    success = False
            break
    return resp, success


def get_jac_pinv_plan(
        start_joint_state,
        start_pose,
        end_pose,
        link_name,
        velocity=0.2,
        dt=0.1,
        n_steps=0
):
    """
    Convenience function to request a Cartesian plan using the Jacobian
    pseudoinverse planner.

    Args:
        start_joint_state (JointState): Initial joint state of the robot (need at least
                                        joint positions and joint names)
        start_pose (Pose): Initial EE pose
        end_pose (Pose): Terminal EE pose
        link_name (str): Name of link the poses are for
        velocity (float): Cartesian velocity of the motion
    Returns:
        resp (GetJacobianPInvPlanResponse): Service response
        success (bool): Boolean that is true if service call was successful
    """
    req = GetJacobianPInvPlanRequest()
    req.start_joints = start_joint_state
    req.start_ee_pose = start_pose
    req.end_ee_pose = end_pose
    req.link_name = link_name
    req.velocity = velocity
    req.dt = dt
    req.n_steps = n_steps
    return ros_util.call_service(req, GET_JAC_PINV_PLAN_SRV, GetJacobianPInvPlan)


def get_fk(joints, joint_names, link_name):
    req = GetFKRequest()
    req.link_name = link_name
    for joint_vals in joints:
        js = JointState()
        js.position = joint_vals
        js.name = joint_names
        req.joints.append(js)
    return ros_util.call_service(req, GET_FK_SRV, GetFK)


def get_jacobian(joints, joint_names, link_name):
    req = GetJacobianRequest()
    req.link_name = link_name
    for joint_vals in joints:
        js = JointState()
        js.position = joint_vals
        js.name = joint_names
        req.joints.append(js)
    return ros_util.call_service(req, GET_JAC_SRV, GetJacobian)


def get_joint_info():
    req = GetJointInfoRequest()
    return ros_util.call_service(req, GET_JOINT_INFO_SRV, GetJointInfo)


def get_ik(poses, link_name, n_attempts=10, timeout=0.1):
    req = GetIKRequest()
    for pose in poses:
        if isinstance(pose, Pose):
            req.poses.append(pose)
        elif isinstance(pose, list) or isinstance(pose, tuple) or isinstance(pose, np.ndarray):
            p = Pose()
            p.position.x = pose[0]
            p.position.y = pose[1]
            p.position.z = pose[2]
            p.orientation.x = pose[3]
            p.orientation.y = pose[4]
            p.orientation.z = pose[5]
            p.orientation.w = pose[6]
            req.poses.append(p)
    req.link_name = link_name
    req.n_attempts = n_attempts
    req.timeout = timeout
    return ros_util.call_service(req, GET_IK_SRV, GetIK)


def get_planning_scene(joint_state=None, objects={}):
    """
    Creates a PlanningScene instance for collision checking in MoveIt.

    Args:
        joint_state (JointState): Robot joint state (optional)
        objects (dict): Dictionary with object name keys and object configuration values.
                        Should specify everything needed to create the collision objects
                        for each object.
    """
    planning_scene = PlanningScene()

    if joint_state:
        planning_scene.robot_state.joint_state = joint_state

    for obj_name, obj_config in objects.items():
        collision_obj = CollisionObject()
        collision_obj.header.frame_id = obj_config['frame_id']
        collision_obj.id = obj_name
        collision_obj.operation = obj_config['operation']
        if obj_config['object_type'] == 'box':
            prim = get_box_prim(obj_config['extents'])
            collision_obj.primitives.append(prim)
            collision_obj.primitive_poses.append(obj_config['pose'])
        elif obj_config['object_type'] in ["urdf", "random", "random_placeable", "random_placee"]:
            asset_root = ros_util.resolve_ros_package_path(obj_config['asset_root'])
            objs, poses = urdf_to_collision(
                obj_config['asset_filename'],
                obj_config['pose'],
                asset_root
            )
            for obj, pose in zip(objs, poses):
                if isinstance(obj, SolidPrimitive):
                    collision_obj.primitives.append(obj)
                    collision_obj.primitive_poses.append(pose)
                elif isinstance(obj, Mesh):
                    collision_obj.meshes.append(obj)
                    collision_obj.mesh_poses.append(pose)

            obj_color = ObjectColor()
            obj_color.color.r = 0.67
            obj_color.color.g = 1.0
            obj_color.color.b = 0.5
            obj_color.color.a = 1.0
            obj_config['color'] = obj_color.color
        else:
            raise ValueError("Unknown object type for collision object: {}"
                             "".format(obj_config['object_type']))

        if 'attached_pose' in obj_config and 'attach_to_link' in obj_config:
            attached_obj = AttachedCollisionObject()
            attached_obj.object.header.frame_id = obj_config['attach_to_link']
            attached_obj.object.id = obj_name
            attached_obj.object.primitives = deepcopy(collision_obj.primitives)
            attached_obj.object.meshes = deepcopy(collision_obj.meshes)
            if attached_obj.object.primitives:
                attached_obj.object.primitive_poses = [obj_config['attached_pose']]
            elif attached_obj.object.meshes:
                attached_obj.object.mesh_poses = [obj_config['attached_pose']]
            attached_obj.object.operation = CollisionObject.ADD
            attached_obj.link_name = obj_config['attach_to_link']
            attached_obj.touch_links = obj_config['touch_links']
            planning_scene.robot_state.attached_collision_objects.append(attached_obj)
            # Remove object from world collisions since it's now attached to robot
            collision_obj.operation = CollisionObject.REMOVE

        planning_scene.world.collision_objects.append(collision_obj)

        if 'color' in obj_config:
            obj_color = ObjectColor()
            obj_color.id = obj_name
            obj_color.color = obj_config['color']
            planning_scene.object_colors.append(obj_color)

    return planning_scene

def reset_planning_scene():
    """
    Call this function to reset Moveit Planning scene in Rviz
    """
    req = PlanningSceneComponents()
    cur_planning_scene = PlanningScene()

    cur_planning_scene, _ = ros_util.call_service(req, GET_PLANNING_SCENE_SRV, GetPlanningScene)
    cur_planning_scene.scene.world.collision_objects = []
    _ = ros_util.call_service(cur_planning_scene.scene, APPLY_PLANNING_SCENE_SRV, ApplyPlanningScene)

def urdf_to_collision(asset_filename, obj_pose, asset_root=DEFAULT_ASSET_ROOT):
    """
    Converts URDF specification into a collection of SolidPrimitive objects.

    Args:
        asset_filename (str): Absolute path to URDF file
        obj_pose (Pose): Pose of object relative to environment reference frame (e.g. 'world')
    Returns:
        prims (list): List of SolidPrimitive objects representing collision geometry
        prim_poses (list): List of Pose objects in same reference frame as base link
    """
    if not asset_root:
        asset_root = DEFAULT_ASSET_ROOT
    asset_path = os.path.join(asset_root, asset_filename)
    file_util.check_path_exists(asset_path, "URDF file")
    robot = URDF.load(asset_path)
    collision_fk = robot.collision_geometry_fk()

    prims = []
    prim_poses = []
    for collision, tf in collision_fk.items():
        # TODO will need to add other geometry types (cylinder, sphere, mesh)
        if collision.geometry.box:
            prim = get_box_prim(collision.geometry.box.size)
        elif collision.geometry.cylinder:
            prim = get_cylinder_prim(collision.geometry.cylinder.radius,
                                     collision.geometry.cylinder.length)
        elif collision.geometry.mesh:
            mesh = collision.geometry.mesh.meshes[0] # I think there will only be 1 per collision?
            verts = mesh.vertices.view(np.ndarray)
            faces = mesh.faces.view(np.ndarray)
            prim = get_mesh(verts, faces)
        else:
            raise ValueError(f"Unsupported URDF collision geometry type: {collision.geometry}")
        prims.append(prim)

        # Compute global pose of link using world_to_obj and obj_to_link TFs
        base_tf = ros_util.pose_to_homogeneous(obj_pose)
        link_tf = np.dot(base_tf, tf)
        prim_pose = ros_util.homogeneous_to_pose(link_tf)
        prim_poses.append(prim_pose)
    return prims, prim_poses


def get_box_prim(dims):
    """
    Constructs a SolidPrimitive of type BOX.
    """
    prim = SolidPrimitive()
    prim.type = prim.BOX
    prim.dimensions = dims
    return prim


def get_cylinder_prim(radius, length):
    """
    Constructs a SolidPrimitive of type CYLINDER.
    """
    prim = SolidPrimitive()
    prim.type = prim.CYLINDER
    prim.dimensions = [length, radius] # Order defined on SolidPrimitive.msg
    return prim


def get_mesh(verts, faces):
    prim = Mesh()
    for v in verts:
        prim.vertices.append(Point(*v))
    for f in faces:
        prim.triangles.append(MeshTriangle(f.tolist()))
    return prim


if __name__ == '__main__':
    from geometry_msgs.msg import Pose

    asset_filename = os.path.join(ros_util.get_path("ll4ma_robots_description"), "urdf",
                                  "environment", "static", "basket.urdf")
    pose = Pose()
    pose.orientation.w = 1
    print(urdf_to_collision(asset_filename, pose))
