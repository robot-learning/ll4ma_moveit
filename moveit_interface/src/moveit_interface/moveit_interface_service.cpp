#include <moveit_interface/moveit_interface_service.h>
#include <eigen_conversions/eigen_msg.h>
#include <Eigen/Geometry>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>

#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <moveit_msgs/OrientationConstraint.h>
#include <moveit_msgs/Constraints.h>

using namespace moveit_interface;
using namespace moveit_msgs;

#define DEFAULT_PLANNING_TIME 5.0


MoveItInterfaceService::MoveItInterfaceService(const ros::NodeHandle& nh) : nh_(nh) {}


bool MoveItInterfaceService::init()
{
  std::string planning_group, joint_state_topic;
  bool wait_for_joint_state;

  if (!nh_.getParam("planning_group", planning_group))
  {
    ROS_ERROR("Could not read planning group from parameter server");
    return false;
  }

  nh_.param<bool>("wait_for_joint_state", wait_for_joint_state, false);
  if (wait_for_joint_state)
  {
    // Wait for joint state before creating move group, has fatal timeout for it
    nh_.param<std::string>("joint_state_topic", joint_state_topic, "joint_states");
    ROS_INFO_STREAM("Waiting for robot joint state on topic: " << joint_state_topic);
    ros::Duration timeout(10.0);
    if (!ros::topic::waitForMessage<sensor_msgs::JointState>(joint_state_topic, nh_, timeout))
    {
      ROS_ERROR("Joint state not received. Necessary for MoveGroup.");
      return false;
    }
    else
    {
      ROS_INFO("Joint state in MoveItInterfaceService received");
    }
  }

  move_group_ = std::make_shared<moveit::planning_interface::MoveGroupInterface>(planning_group);
  robot_model_loader_ = std::make_shared<robot_model_loader::RobotModelLoader>("robot_description");
  robot_model_ = robot_model_loader_->getModel();
  robot_state_ = std::make_shared<moveit::core::RobotState>(robot_model_);
  joint_group_ = robot_model_->getJointModelGroup(move_group_->getName());

  // Services offered by this node
  get_plan_srv_ = nh_.advertiseService("get_plan", &MoveItInterfaceService::getPlan, this);
  get_jac_pinv_plan_srv_ = nh_.advertiseService("get_jacobian_pinv_plan", &MoveItInterfaceService::getJacobianPInvPlan, this);
  update_env_srv_ = nh_.advertiseService("update_env", &MoveItInterfaceService::updateEnvironment, this);
  get_ik_srv_ = nh_.advertiseService("get_ik", &MoveItInterfaceService::getIK, this);
  get_fk_srv_ = nh_.advertiseService("get_fk", &MoveItInterfaceService::getFK, this);
  get_jacobian_srv_ = nh_.advertiseService("get_jacobian", &MoveItInterfaceService::getJacobian, this);
  get_joint_info_srv_ = nh_.advertiseService("get_joint_info", &MoveItInterfaceService::getJointInfo, this);

  return true;
}


bool MoveItInterfaceService::updateEnvironment(
  moveit_interface::GetPlan::Request& req,
  moveit_interface::GetPlan::Response& resp)
{
  ROS_DEBUG("updateEnvironment triggered");
  if (req.planning_scene.world.collision_objects.size() > 0){
    planning_scene_.applyCollisionObjects(req.planning_scene.world.collision_objects,
                                          req.planning_scene.object_colors);
    ROS_DEBUG_STREAM("A collision object was added with frame_id: "
                     << req.planning_scene.world.collision_objects[0].header.frame_id);
  }

  if (req.planning_scene.robot_state.attached_collision_objects.size() > 0)
  {
    ROS_DEBUG("An attached collision object added");
    planning_scene_.applyAttachedCollisionObjects(req.planning_scene.robot_state.attached_collision_objects);
  }

  resp.success = false;
  return true;
}


bool MoveItInterfaceService::getPlan(
  moveit_interface::GetPlan::Request& req,
  moveit_interface::GetPlan::Response& resp)
{
  ROS_DEBUG("getPlan triggered");

  if (req.planning_scene.world.collision_objects.size() > 0){
    planning_scene_.applyCollisionObjects(req.planning_scene.world.collision_objects,
                                          req.planning_scene.object_colors);
    ROS_DEBUG_STREAM("A collision object was added with frame_id: "
                     << req.planning_scene.world.collision_objects[0].header.frame_id);
  }

  if (req.planning_scene.robot_state.joint_state.position.size() > 0)
  {
    // Need to do this check since MoveIt will just give an uninformative error
    // message and then silently use the current joint state
    if (req.planning_scene.robot_state.joint_state.name.size() !=
        req.planning_scene.robot_state.joint_state.position.size())
    {
      ROS_ERROR_STREAM("Must specify joint names corresponding to joint positions when "
                       << " overriding current joint state for planning" << req.planning_scene.robot_state.joint_state.name.size() << req.planning_scene.robot_state.joint_state.position.size());
      resp.success = false;
      return true;
    }

    move_group_->setStartState(req.planning_scene.robot_state);
  }
  else
  {
    ROS_DEBUG("Computing plan from current joint state");
  }

  move_group_->setMaxVelocityScalingFactor(req.max_vel_factor);
  move_group_->setMaxAccelerationScalingFactor(req.max_acc_factor);
  if (req.planning_time > 0)
    move_group_->setPlanningTime(req.planning_time);
  else
    move_group_->setPlanningTime(DEFAULT_PLANNING_TIME);

  if (!req.plannerId.empty())
    move_group_->setPlannerId(req.plannerId);

  if (req.cartesian_path)
  {
    move_group_->setEndEffectorLink(req.end_effector_frame);
    move_group_->setPoseReferenceFrame("world");

    std::vector<geometry_msgs::Pose> waypoints;
    waypoints.push_back(req.target_pose.pose);
    // TODO not sure what the best value for jump threshold is, tried value
    // from here: https://thomasweng.com/moveit_cartesian_jump_threshold/
    const double jump_threshold = (req.jump_threshold > 0) ? req.jump_threshold : 5.0;
    const double eef_step = (req.eef_step > 0) ? req.eef_step : 0.01;
    resp.cartesian_path_pct = move_group_->computeCartesianPath(waypoints, eef_step,
                                                                jump_threshold,
                                                                resp.trajectory);
    resp.success = true;
  }
  else
  {
    if (req.go_to_pose)
    {
      if (!req.named_target.empty())
        move_group_->setNamedTarget(req.named_target);
      else
        move_group_->setPoseTarget(req.target_pose, req.end_effector_frame);
    }
    else if(req.go_to_configuration)
    {
      move_group_->setJointValueTarget(req.target_configuration);
    }
    else
    {
      ROS_ERROR("Must set either go_to_pose or go_to_configuration");
    }

    try
    {
      // moveit_msgs::Constraints test_constraints;
      bool plan_succ = (move_group_->plan(plan_) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
      if (plan_succ)
      {
        resp.start_state = plan_.start_state_;
        resp.trajectory = plan_.trajectory_;
        resp.success = true;
      }
      else
      {
        ROS_ERROR("Plan request failed");
        resp.success = false;
      }
    }
    catch (const std::exception &exc)
    {
      std::cerr << exc.what();
      ROS_ERROR("Plan request failed");
      resp.success = false;
    }

  }

  return true;
}


bool MoveItInterfaceService::getJacobianPInvPlan(
  moveit_interface::GetJacobianPInvPlan::Request& req,
  moveit_interface::GetJacobianPInvPlan::Response& resp)
{
  if (!req.velocity > 0)
  {
    ROS_ERROR("Must specify positive values for velocity in request");
    return false;
  }

  // Read off start/end poses into Eigen types
  Eigen::Vector3d start_pos, end_pos;
  Eigen::Quaterniond start_quat, end_quat;
  tf::pointMsgToEigen(req.start_ee_pose.position, start_pos);
  tf::pointMsgToEigen(req.end_ee_pose.position, end_pos);
  tf::quaternionMsgToEigen(req.start_ee_pose.orientation, start_quat);
  tf::quaternionMsgToEigen(req.end_ee_pose.orientation, end_quat);

  // Compute number of steps and duration based on distance traveled, velocity, and timestep
  double n_steps, duration, dist;
  if (req.n_steps > 0) {
    n_steps = req.n_steps;
  } else {
    dist = (end_pos - start_pos).norm();
    duration = dist / req.velocity;
    n_steps = duration / req.dt;
  }


  // Linearly interpolate between the start/end poses (SLERP for quaternions)
  double interp_t = 0.0;
  std::vector<Eigen::Vector3d> pts = {start_pos};
  std::vector<Eigen::Quaterniond> quats = {start_quat};
  for (int i = 0; i < n_steps - 1; ++i)  // Exclude last step, we'll just append at end
  {
    interp_t += 1.0 / n_steps;
    pts.push_back((1.0 - interp_t) * start_pos + interp_t * end_pos);
    quats.push_back(start_quat.slerp(interp_t, end_quat));
  }
  quats.push_back(end_quat);
  pts.push_back(end_pos);

  // Compute joint trajectory corresponding to EE pose trajectory
  resp.trajectory.joint_trajectory.joint_names = req.start_joints.name;
  int n_joints = req.start_joints.position.size();
  Eigen::VectorXd x_diff, joints_diff;
  Eigen::MatrixXd J, J_pinv;
  x_diff.setZero(6);
  joints_diff.setZero(n_joints);
  J.setZero(6, n_joints);
  J_pinv.setZero(n_joints, 6);
  sensor_msgs::JointState current_joints = req.start_joints;
  trajectory_msgs::JointTrajectoryPoint prev_pt;
  prev_pt.positions = req.start_joints.position;
  moveit::core::JointBoundsVector bounds = robot_model_->getActiveJointModelsBounds();
  for (int i = 1; i < pts.size(); ++i)
  {
    // Solve diffs between consecutive poses
    x_diff.head(3) = pts[i] - pts[i-1];
    // For quats, want to solve diff * q1 = q2 --> diff = q2 * inverse(q1) where
    // inverse is conjugate since we'll assume these are unit quaternions

    // TODO for now not computing rots and leaving zeros, assumes fixed orientation start/end
    //   Note: more care needs to be taken here because you can get equivalent solutions that
    //         are actually big changes (i.e. flipping axes by +/- pi)
    // x_diff.tail(3) = (quats[i] * quats[i-1].conjugate()).toRotationMatrix().eulerAngles(0, 1, 2);

    // Get the Jacobian for the current joint configuration
    getJacobian(current_joints, req.link_name, J);

    // Compute joints diff using Jacobian pseudoinverse and computed pose diff
    getPseudoInverse(J, J_pinv, 1.e-5);
    joints_diff = J_pinv * x_diff;
    ROS_INFO_STREAM("Diff shape " << joints_diff.size());
    ROS_INFO_STREAM("Prev shape " << prev_pt.positions.size());

    // Add delta to get the current joint positions
    trajectory_msgs::JointTrajectoryPoint pt;
    double joint_val;
    for (int j = 0; j < joints_diff.size(); ++j)
    {
      joint_val = prev_pt.positions[j] + joints_diff[j];
      // Check if value is within joint limits, fail if it exceeds them
      if (joint_val > bounds.at(j)->at(0).max_position_ ||
          joint_val < bounds.at(j)->at(0).min_position_)
      {
        ROS_WARN_STREAM("Joint " << req.start_joints.name[j] << " exceeded joint limits "
                        << "while computing Cartesian plan");
        resp.success = false;
        return true;
      }
      pt.positions.push_back(joint_val);
    }
    pt.time_from_start = ros::Duration(prev_pt.time_from_start.toSec() + req.dt);

    current_joints.position = pt.positions;
    resp.trajectory.joint_trajectory.points.push_back(pt);
    prev_pt = pt;
  }

  // TODO can also add nullspace resolution

  // TODO might be good to pass result through smoother, have to see if
  // there's a nice way to handle that through moveit without having
  // to externally import the smoothing code from tobias' repo.

  resp.success = true;
  return true;
}


bool MoveItInterfaceService::getIK(
  moveit_interface::GetIK::Request& req,
  moveit_interface::GetIK::Response& resp)
{
  int n_poses = req.poses.size();
  resp.solutions.resize(n_poses);
  resp.solution_found.resize(n_poses);

  robot_state::JointModelGroup* s_joint_group_;
  std::string l_list;
  for (int i = 0; i < req.link_name.find('_'); i++){l_list += req.link_name[i];}

  if (l_list.compare("left") == 0){
    s_joint_group_ = robot_model_->getJointModelGroup("left_arm");}
  else if (l_list.compare("right") == 0){
    s_joint_group_ = robot_model_->getJointModelGroup("right_arm");}
  else {
    s_joint_group_ = robot_model_->getJointModelGroup("iiwa_arm");}

  for (std::size_t i = 0; i < n_poses; ++i)
  {
    resp.solution_found[i] = robot_state_->setFromIK(
      s_joint_group_, req.poses[i], req.link_name, req.n_attempts, req.timeout
    );
    if (resp.solution_found[i])
    {
      robot_state_->copyJointGroupPositions(
        s_joint_group_, resp.solutions[i].position
      );
      resp.solutions[i].name = s_joint_group_->getVariableNames();
    }
  }

  resp.success = true;
  return true;
}


bool MoveItInterfaceService::getFK(
  moveit_interface::GetFK::Request& req,
  moveit_interface::GetFK::Response& resp)
{
  resp.poses.resize(req.joints.size());

  for (std::size_t i = 0; i < req.joints.size(); ++i)
  {
    robot_state_->setVariableValues(req.joints[i]);
    const Eigen::Isometry3d& pose = robot_state_->getGlobalLinkTransform(req.link_name);
    tf::poseEigenToMsg(pose, resp.poses[i]);
  }

  resp.success = true;
  return true;
}


void MoveItInterfaceService::getJacobian(
  const sensor_msgs::JointState joint_state,
  const std::string link_name,
  Eigen::MatrixXd& jacobian)
{
  robot_state::JointModelGroup* s_joint_group_;
  std::string l_list;
  for (int i = 0; i < link_name.find('_'); i++){l_list += link_name[i];}

  if (l_list.compare("left") == 0){
    s_joint_group_ = robot_model_->getJointModelGroup("left_arm");}
  else if (l_list.compare("right") == 0){
    s_joint_group_ = robot_model_->getJointModelGroup("right_arm");}
  else {
    s_joint_group_ = robot_model_->getJointModelGroup("iiwa_arm");}

  robot_state_->setVariableValues(joint_state);
  Eigen::Vector3d reference_point_position(0.0, 0.0, 0.0);
  robot_state_->getJacobian(
    s_joint_group_,
    robot_state_->getLinkModel(link_name),
    reference_point_position,
    jacobian);
}


bool MoveItInterfaceService::getJacobian(
  moveit_interface::GetJacobian::Request& req,
  moveit_interface::GetJacobian::Response& resp)
{
  resp.jacobian.resize(req.joints.size());

  Eigen::MatrixXd jacobian;
  for (std::size_t i = 0; i < req.joints.size(); ++i)
  {
    getJacobian(req.joints[i], req.link_name, jacobian);
    tf::matrixEigenToMsg(jacobian, resp.jacobian[i]);
  }

  resp.success = true;
  return true;
}


bool MoveItInterfaceService::getJointInfo(
  moveit_interface::GetJointInfo::Request& req,
  moveit_interface::GetJointInfo::Response& resp)
{
  moveit::core::JointBoundsVector bounds = robot_model_->getActiveJointModelsBounds();
  resp.upper_position.resize(bounds.size());
  resp.lower_position.resize(bounds.size());
  resp.upper_velocity.resize(bounds.size());
  resp.lower_velocity.resize(bounds.size());
  for (std::size_t i = 0; i < bounds.size(); ++i)
  {
    resp.upper_position[i] = bounds.at(i)->at(0).max_position_;
    resp.lower_position[i] = bounds.at(i)->at(0).min_position_;
    resp.upper_velocity[i] = bounds.at(i)->at(0).max_velocity_;
    resp.lower_velocity[i] = bounds.at(i)->at(0).min_velocity_;
  }

  std::vector<std::string> joint_names = joint_group_->getLinkModelNames();
  resp.joint_names.resize(joint_names.size());
  for (std::size_t i = 0; i < joint_names.size(); ++i)
  {
    resp.joint_names[i] = joint_names[i];
  }

  std::string urdf;
  nh_.getParam("/robot_urdf", urdf);
  // ROS_INFO_STREAM("URDF: " << urdf);
  resp.urdf = urdf;

  resp.success = true;
  return true;
}


void MoveItInterfaceService::getPseudoInverse(
  const Eigen::MatrixXd &m,
  Eigen::MatrixXd &m_pinv,
  const double tolerance)
{
  Eigen::JacobiSVD<Eigen::MatrixXd> svd(m, Eigen::ComputeFullU | Eigen::ComputeFullV);
  Eigen::JacobiSVD<Eigen::MatrixXd>::SingularValuesType sing_vals = svd.singularValues();
  // set values within tolerance to zero
  for (std::size_t idx = 0; idx < sing_vals.size(); idx++)
  {
    if (tolerance > 0.0 && sing_vals(idx) > tolerance)
      sing_vals(idx) = 1.0 / sing_vals(idx);
    else
      sing_vals(idx) = 0.0;
  }

  m_pinv = svd.matrixV().leftCols(sing_vals.size()) * sing_vals.asDiagonal() * svd.matrixU().leftCols(sing_vals.size()).transpose();
}



int main(int argc, char** argv)
{
  ros::init(argc, argv, "moveit_interface_service");
  ros::AsyncSpinner spinner(4);
  spinner.start();

  ros::NodeHandle nh("~");

  MoveItInterfaceService moveit_interface(nh);
  if (!moveit_interface.init())
  {
    ROS_ERROR("Could not initialize the MoveIt interface");
    return 1;
  }
  else
  {
    ROS_INFO("MoveIt interface services are available");
  }

  ros::waitForShutdown();

  return 0;
}
