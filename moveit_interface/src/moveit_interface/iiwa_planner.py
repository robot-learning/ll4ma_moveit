from __future__ import print_function

import rospy
import numpy as np
import multiprocessing as mp
import matplotlib.pyplot as plt
from sensor_msgs.msg import JointState
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint

from ll4ma_util import ui_util, ros_util
from moveit_interface import util as moveit_util


class IiwaPlanner:

    def __init__(self, rate=10, robot_name='iiwa', ee_link='reflex_palm_link', ns=''):
        self.rate = rospy.Rate(rate)
        self.robot_name = robot_name
        self.ee_link = ee_link
        self.ns = ns
        if len(ns) > 0:
            self.ns = '/'+ns
        self.joint_state = None
        self.create_publishers_and_subscribers()

    def create_publishers_and_subscribers(self):
        self.cmd_pub = rospy.Publisher("{}/{}/joint_cmd".format(self.ns, self.robot_name), JointState, queue_size=1)
        rospy.Subscriber("{}/{}/joint_states".format(self.ns, self.robot_name), JointState, self._joint_state_cb)

    def get_plan(self, pose_name, objects={}, vel_scaling=0.1, acc_scaling=0.1):
        traj = None
        dt = self.rate.sleep_dur.to_sec()
        resp, success = moveit_util.get_plan(pose_name, self.joint_state, self.ee_link,
                                             objects=objects, max_vel_factor=vel_scaling, max_acc_factor=acc_scaling)
        if success:
            traj = ros_util.interpolate_joint_trajectory(resp.trajectory.joint_trajectory, dt)
        return traj

    def command_trajectory(self, traj, plot=False, at_goal_tolerance=0.01,
                           wait_secs_for_at_goal=0, wait_secs_after_commanded=0,
                           preview=True):
        if traj is None:
            rospy.logwarn("No trajectory to command")
            return False
        if not self.display_rviz_trajectory(traj):
            ui_util.print_error_exit("Could not visualize trajectory, NOT executing trajectory")
        if preview and not ui_util.query_yes_no("\nExecute trajectory?", default="yes"):
            ui_util.print_info_exit("\n    Trajectory is NOT being executed\n\n")
            return False

        self.actual_pos = []
        self.actual_vel = []
        self.commanded_pos = []
        self.commanded_vel = []
        self.time = []
        cmd = JointState()
        cmd.name = ["iiwa_joint_{}".format(i) for i in range(1,8)]

        rospy.loginfo("Commanding trajectory with {} waypoints at rate {}Hz..."
                      "".format(len(traj.points), 1./self.rate.sleep_dur.to_sec()))
        for point in traj.points:
            cmd.position = point.positions
            cmd.velocity = point.velocities
            cmd.effort = point.accelerations
            cmd.header.stamp = rospy.Time.now()
            self.cmd_pub.publish(cmd)
            if plot:
                self.actual_pos.append(self.joint_state.position)
                self.actual_vel.append(self.joint_state.velocity)
                self.commanded_pos.append(point.positions)
                self.commanded_vel.append(point.velocities)
                self.time.append(point.time_from_start.to_sec())
            self.rate.sleep()

        if wait_secs_for_at_goal > 0:
            rospy.loginfo("Waiting for robot to reach target joint position...")
            start = rospy.get_time()
            target = np.array(cmd.position)
            while not rospy.is_shutdown() and \
                  rospy.get_time() - start < wait_secs_for_at_goal and \
                  np.linalg.norm(target - np.array(self.joint_state.position)) > at_goal_tolerance:
                self.rate.sleep()
            if np.linalg.norm(target - np.array(self.joint_state.position)) > at_goal_tolerance:
                rospy.logwarn("Robot did not fully reach target joint position")
            else:
                rospy.loginfo("Robot has reached target joint position!")
        elif wait_secs_after_commanded > 0:
            rospy.loginfo("Waiting {} seconds...".format(wait_secs_after_commanded))
            start = rospy.get_time()
            time = point.time_from_start.to_sec()
            while not rospy.is_shutdown() and \
                  rospy.get_time() - start < wait_secs_after_commanded:
                if plot:
                    self.actual_pos.append(self.joint_state.position)
                    self.actual_vel.append(self.joint_state.velocity)
                    self.commanded_pos.append(point.positions)
                    self.commanded_vel.append(point.velocities)
                    self.time.append(time + rospy.get_time() - start)
                self.rate.sleep()
            rospy.loginfo("Commanding complete")
        else:
            rospy.loginfo("Commanding complete")

        if plot:
            self.plot_trajectory()
        return True

    def set_rate(self, rate):
        self.rate = rospy.Rate(rate)

    def display_rviz_trajectory(self, traj):
        return ros_util.display_rviz_trajectory(traj, self.joint_state, self.robot_name, \
                                                "{}/display_trajectory".format(self.ns))

    def plot_trajectory(self):
        actual_pos = np.array(self.actual_pos)
        actual_vel = np.array(self.actual_vel)
        commanded_pos = np.array(self.commanded_pos)
        commanded_vel = np.array(self.commanded_vel)

        fig, axes = plt.subplots(2, 7)
        fig.set_size_inches(18, 6)
        for i in range(7):
            axes[0,i].plot(self.time, commanded_pos[:,i])
            axes[0,i].plot(self.time, actual_pos[:,i])
            axes[0,i].set_title("Joint {}".format(i+1))
            axes[1,i].plot(self.time, commanded_vel[:,i])
            axes[1,i].plot(self.time, actual_vel[:,i])
        plt.tight_layout()
        plt.show()

    def wait_for_joint_state(self, timeout=10.0):
        if self.joint_state is None:
            rospy.loginfo("Waiting for joint state...")
            start = rospy.get_time()
            while not rospy.is_shutdown() and self.joint_state is None and \
                  rospy.get_time() - start < timeout:
                self.rate.sleep()
            if self.joint_state is not None:
                rospy.loginfo("Joint state received")
            else:
                rospy.logerr("Joint state unknown")
                return False
        return True

    def _joint_state_cb(self, msg):
        self.joint_state = msg



class BiIiwaPlanner(IiwaPlanner):
    def __init__(self, rate=10, robot_name='bimanual_iiwa', ee_link='reflex_palm_link'):
        super().__init__(rate, robot_name='bimanual_iiwa', ee_link='reflex_palm_link')
        self.left_iiwa = IiwaPlanner(ns = 'left')
        self.right_iiwa = IiwaPlanner(ns = 'right')

    def create_publishers_and_subscribers(self):
        rospy.Subscriber("/joint_states", JointState, self._joint_state_cb)

    def get_plan(self, pose_name, objects={}, vel_scaling=0.1, acc_scaling=0.1):
        raise NotImplementedError('This needs to be updated for bimanual robots')

    def _command_inner(args):
        ns, robot_name, traj, rate = args
        cmd_pub = rospy.Publisher("{}/{}/joint_cmd".format(ns, robot_name), JointState, queue_size=1)
        rospy.loginfo("Publishing over {}/{}/joint_cmd".format(ns, robot_name))
        cmd = JointState()
        cmd.name = ["iiwa_joint_{}".format(i) for i in range(1,8)]

        rospy.loginfo("Commanding trajectory with {} waypoints at rate {}Hz..."
                      "".format(len(traj.points), 1./rate.sleep_dur.to_sec()))
        for point in traj.points:
            cmd.position = point.positions
            cmd.velocity = point.velocities
            cmd.effort = point.accelerations
            cmd.header.stamp = rospy.Time.now()
            cmd_pub.publish(cmd)
            # if plot:
            #     self.actual_pos.append(self.joint_state.position)
            #     self.actual_vel.append(self.joint_state.velocity)
            #     self.commanded_pos.append(point.positions)
            #     self.commanded_vel.append(point.velocities)
            #     self.time.append(point.time_from_start.to_sec())
            rate.sleep()

    def command_trajectory(self, traj, plot=False, at_goal_tolerance=0.01,
                           wait_secs_for_at_goal=0, wait_secs_after_commanded=0,
                           preview=True):
        if traj is None:
            rospy.logwarn("No trajectory to command")
            return False
        left_traj, right_traj = split_trajectory(traj)
        if not self.display_rviz_trajectory(left_traj, right_traj):
            ui_util.print_error_exit("Could not visualize trajectory, NOT executing trajectory")
            return False
        if preview and not ui_util.query_yes_no("\nExecute trajectory?", default="yes"):
            ui_util.print_info_exit("\n    Trajectory is NOT being executed\n\n")
            return False
        
        # self.actual_pos = []
        # self.actual_vel = []
        # self.commanded_pos = []
        # self.commanded_vel = []
        # self.time = []

        l_cmd = JointState()
        l_cmd.name = ["iiwa_joint_{}".format(i) for i in range(1,8)]

        r_cmd = JointState()
        r_cmd.name = ["iiwa_joint_{}".format(i) for i in range(1,8)]

        rospy.loginfo("Commanding trajectory with {} waypoints at rate {}Hz..."
                      "".format(len(traj.points), 1./self.rate.sleep_dur.to_sec()))
        for i in range(len(traj.points)):
            
            l_cmd.position = left_traj.points[i].positions
            l_cmd.velocity = left_traj.points[i].velocities
            l_cmd.effort = left_traj.points[i].accelerations

            r_cmd.position = right_traj.points[i].positions
            r_cmd.velocity = right_traj.points[i].velocities
            r_cmd.effort = right_traj.points[i].accelerations
            
            l_cmd.header.stamp = rospy.Time.now()
            r_cmd.header.stamp = l_cmd.header.stamp
            
            self.left_iiwa.cmd_pub.publish(l_cmd)
            self.right_iiwa.cmd_pub.publish(r_cmd)
            # if plot:
            #     self.actual_pos.append(self.joint_state.position)
            #     self.actual_vel.append(self.joint_state.velocity)
            #     self.commanded_pos.append(point.positions)
            #     self.commanded_vel.append(point.velocities)
            #     self.time.append(point.time_from_start.to_sec())
            self.rate.sleep()

        if plot:
            rospy.logwarn("Plot feature is unavailable currently with bimanual setup")
        
        if wait_secs_for_at_goal > 0:
            rospy.loginfo("Waiting for robot to reach target joint position...")
            rospy.logwarn("Feature is unavailable currently with bimanual setup")
            # start = rospy.get_time()
            # target = np.array(cmd.position)
            # while not rospy.is_shutdown() and \
            #       rospy.get_time() - start < wait_secs_for_at_goal and \
            #       np.linalg.norm(target - np.array(self.joint_state.position)) > at_goal_tolerance:
            #     self.rate.sleep()
            # if np.linalg.norm(target - np.array(self.joint_state.position)) > at_goal_tolerance:
            #     rospy.logwarn("Robot did not fully reach target joint position")
            # else:
            #     rospy.loginfo("Robot has reached target joint position!")
        elif wait_secs_after_commanded > 0:
            rospy.loginfo("Waiting {} seconds...".format(wait_secs_after_commanded))
            rospy.logwarn("Feature is unavailable currently with bimanual setup")
            # start = rospy.get_time()
            # time = point.time_from_start.to_sec()
            # while not rospy.is_shutdown() and \
            #       rospy.get_time() - start < wait_secs_after_commanded:
            #     if plot:
            #         self.actual_pos.append(self.joint_state.position)
            #         self.actual_vel.append(self.joint_state.velocity)
            #         self.commanded_pos.append(point.positions)
            #         self.commanded_vel.append(point.velocities)
            #         self.time.append(time + rospy.get_time() - start)
            #     self.rate.sleep()
            rospy.loginfo("Commanding complete")
        else:
            rospy.loginfo("Commanding complete")

        if plot:
            self.plot_trajectory()
            rospy.logwarn("Plot feature is unavailable currently with bimanual setup")
        return True

    def display_rviz_trajectory(self, left_traj, right_traj):
        left_traj.joint_names = ["left_" + name for name in left_traj.joint_names]
        right_traj.joint_names = ["right_" + name for name in right_traj.joint_names]
        left_succ = ros_util.display_rviz_trajectory(left_traj, self.joint_state, \
                                                     self.robot_name, "/left/display_trajectory")
        right_succ = ros_util.display_rviz_trajectory(right_traj, self.joint_state, \
                                                      self.robot_name, "right/display_trajectory")
        return left_succ and right_succ

    def wait_for_joint_state(self, timeout=10.0):
        return self.left_iiwa.wait_for_joint_state(2.0) and \
            self.right_iiwa.wait_for_joint_state(2.0)
    

def split_trajectory(jtraj):
    left_ids = []
    right_ids = []
    for idx, name in enumerate(jtraj.joint_names):
        if name.startswith('left'):
            left_ids += [idx]
        else:
            right_ids += [idx]

    def split_attribute(attribute):
        numpy_attr_ = np.array(attribute)
        try:
            left_attr = numpy_attr_[left_ids].tolist()
            right_attr = numpy_attr_[right_ids].tolist()
            return left_attr, right_attr
        except:
            return [], []
            
    left_traj = JointTrajectory()
    right_traj = JointTrajectory()
    left_traj.header = jtraj.header
    right_traj.header = jtraj.header
    
    left_traj.joint_names, right_traj.joint_names = split_attribute(jtraj.joint_names)

    for tpoint in jtraj.points:
        left_point = JointTrajectoryPoint()
        right_point = JointTrajectoryPoint()
        left_point.time_from_start = tpoint.time_from_start
        right_point.time_from_start = tpoint.time_from_start
        left_point.positions, right_point.positions = split_attribute(tpoint.positions)
        left_point.velocities, right_point.velocities = split_attribute(tpoint.velocities)
        left_point.accelerations, right_point.accelerations = split_attribute(tpoint.accelerations)
        left_point.effort, right_point.effort = split_attribute(tpoint.effort)
        left_traj.points.append(left_point)
        right_traj.points.append(right_point)
    left_traj.joint_names = [name.replace('left_', '') for name in left_traj.joint_names]
    right_traj.joint_names = [name.replace('right_', '') for name in right_traj.joint_names]          
    return left_traj, right_traj
