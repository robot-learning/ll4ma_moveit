import os, sys
from copy import deepcopy
import numpy as np
import torch

import rospy
import moveit_commander
from moveit_commander.conversions import pose_to_list
from sensor_msgs.msg import JointState
from geometry_msgs.msg import PoseStamped, Pose
from moveit_msgs.msg import (
    Constraints, OrientationConstraint, DisplayTrajectory, PlanningScene
)
from moveit_msgs.msg import CollisionObject as moveit_obj

from ll4ma_util import torch_util, ros_util, math_util


class MoveitPlanner:
    """docstring for MoveitPlanner."""

    def __init__(self, end_effector_frame='reflex_palm_link', real_robot=False):
        self.end_effector_frame = end_effector_frame
        self.ds = 1 / 60.
        self.real_robot = real_robot

        moveit_commander.roscpp_initialize(sys.argv)
        self.robot = moveit_commander.RobotCommander('robot_description')
        self.move_group = moveit_commander.MoveGroupCommander('iiwa_arm')
        self.move_group.set_end_effector_link(self.end_effector_frame)
        self.scene = moveit_commander.PlanningSceneInterface()
        # box_pose = PoseStamped()
        # box_pose.header.frame_id = "world"
        # box_pose.pose.orientation.w = 1.0
        # box_pose.pose.position.x = -0.5
        # box_pose.pose.position.z = 0.2
        # self.scene.add_box('toy', box_pose, size=(0.075, 0.075, 0.075))

        self.set_planner('PRMstar')

        # We can get the name of the reference frame for this robot:
        planning_frame = self.move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)

        # We can also print the name of the end-effector link for this group:
        eef_link = self.move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

        # We can get a list of all the groups in the robot:
        group_names = self.robot.get_group_names()
        print("============ Available Planning Groups:", self.robot.get_group_names())

        # # Sometimes for debugging it is useful to print the entire state of the
        # # robot:
        # print("============ Printing robot state")
        # print(self.robot.get_current_state())
        # print("")

        self._joint_state = self.robot.get_current_state().joint_state
        # self._joint_state = JointState()
        # rospy.Subscriber("/joint_states", JointState, self._joint_cb)

        if not self.real_robot:
            # self._joint_pub = rospy.Publisher('/left/joint_states', JointState, queue_size=1)
            self._joint_pub = rospy.Publisher('/left/iiwa/joint_states', JointState, queue_size=10)

        # self.scene_pub = rospy.Publisher('/iiwa_planning_scene', PlanningScene, queue_size=10)
        #
        # self.scene_service = rospy.Service('/iiwa/get_planning_scene', PlanningScene, self.serve_scene)

        self.display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            DisplayTrajectory,
            queue_size=20,
        )

        self.planscene = {}

        self.clear_collision_env()

        if not self.real_robot:
            self.set_start_joints(np.zeros((7,)))
            self.fk()

    def set_planner(self, planid='PRMstar'):
        self.move_group.set_planner_id(planid)

    def _joint_cb(self, msg):
        self._joint_state = msg

    def set_start_joints(self, start_joints):
        jstate = self.joint_state
        pos = np.array(jstate.position)
        pos[:len(start_joints)] = start_joints[:]
        jstate.position = pos
        if not self.real_robot:
            ros_util.publish_msg(jstate, self._joint_pub, 4, 0.05)
        # print(self.robot.get_current_state())

    @property
    def joint_state(self):
        return deepcopy(self.robot.get_current_state().joint_state)

    def fk(self, joints=None):
        if joints is not None:
            init_j = self.joint_state.position
            self.set_start_joints(joints)
        pose = self.move_group.get_current_pose()
        pose = ros_util.pose_to_homogeneous(pose.pose)
        print(pose)
        if joints is not None:
            self.set_start_joints(init_j)
        return pose

    # def ik(self, pose):
    #     if target.shape == (4, 4):
    #         target = torch_util.transform2quatpose(target)
    #
    #     pose_target = Pose()
    #     pose_target.position.x = target[0]
    #     pose_target.position.y = target[1]
    #     pose_target.position.z = target[2]
    #     pose_target.orientation.x = target[3]
    #     pose_target.orientation.y = target[4]
    #     pose_target.orientation.z = target[5]
    #     pose_target.orientation.w = target[6]
    #
    #

    def clear_collision_env(self):
        planscene = self.scene.get_objects()
        for obj_name in planscene:
            object = planscene[obj_name]
            self.scene.remove_world_object(obj_name)
        planscene = self.scene.get_attached_objects()
        for obj_name in planscene:
            object = planscene[obj_name]
            self.scene.remove_attached_object(object.link_name, name=obj_name)

    def add_collision_env(self, planscene):
        """
        planscene: (moveit_msgs/PlanningScene)
        planscene: (dict)
        """
        self.planscene = planscene
        for obj_name in self.planscene:
            object = self.planscene[obj_name]
            if object['operation'] == moveit_obj.REMOVE:
                self.scene.remove_world_object(obj_name)
            pose = PoseStamped()
            # pose.pose = ros_util.homogeneous_to_pose(object['pose'])
            pose.pose = object['pose']
            pose.header.frame_id = object['frame_id']
            if object['operation'] == moveit_obj.ADD or len(object['touch_links']) > 0:
                self.scene.add_box(obj_name, pose, size=(object['extents']))
            if len(object['touch_links']) > 0:
                self.scene.attach_box(object['frame_id'], obj_name, touch_links=object['touch_links'])

    def get_trajectory(self, target, t_is_pose=True, start_joints=None, level_traj=False, viz=True):
        self.move_group.clear_pose_targets()

        if not self.real_robot and start_joints is not None:
            self.set_start_joints(start_joints)
            self.fk()

        if torch.is_tensor(target):
            target = target.cpu().numpy()
        target = target.astype(np.float64)

        if t_is_pose:
            if target.shape == (4, 4):
                # target = torch_util.transform2quatpose(target)
                pose_target = ros_util.homogeneous_to_pose(target)
            elif target.shape == (7,):
                pose_target = ros_util.array_to_pose(target)
            else:
                assert target.shape == (6,)
                pose_target = ros_util.homogeneous_to_pose(math_util.euler_to_homogeneous(target))
            self.move_group.set_pose_target(pose_target)
        else:
            try:
                self.move_group.set_joint_value_target(target.tolist())
            except Exception as e:
                print(e)
                print('target:', target)
                traj = torch.zeros((3, 0, 7))
                info = {
                    'fk': self.fk(start_joints),
                    'times': torch.zeros((1,)),
                    'ros_traj': None,
                    'ros_plan': None,
                }
                return traj, info

        constraints = Constraints()
        if level_traj:
            oc = OrientationConstraint()
            oc.header.frame_id = 'world'
            oc.link_name = self.end_effector_frame
            oc.orientation = pose_target.orientation
            oc.absolute_x_axis_tolerance = 0.1
            oc.absolute_y_axis_tolerance = 0.9
            oc.absolute_z_axis_tolerance = 0.9
            oc.weight = 1.0
            constraints.orientation_constraints.append(oc)
        self.move_group.set_path_constraints(constraints)
        self.move_group.set_planning_time(15)
        self.move_group.set_num_planning_attempts(1)
        success, plan, plan_time, error_code = self.move_group.plan()

        if viz:
            self.display_trajectory(plan)

        ros_traj = None
        if plan is not None:
            ros_traj = plan.joint_trajectory
        if plan is not None and len(ros_traj.points) > 1:
            ros_traj = ros_util.interpolate_joint_trajectory(ros_traj, self.ds)
            positions = [pt.positions for pt in ros_traj.points]
            velocities = [pt.velocities for pt in ros_traj.points]
            accelerations = [pt.accelerations for pt in ros_traj.points]
            times = torch.tensor([pt.time_from_start.to_sec() for pt in ros_traj.points])
            traj = torch.tensor(np.stack([positions, velocities, accelerations]))
            info = {
                'fk': self.fk(traj[0, -1]),
                'times': times,
                'ros_traj': ros_traj,
                'ros_plan': plan,
            }
        else:
            # print('******** plan came back None ************')
            traj = torch.zeros((3, 0, 7))
            info = {
                'fk': self.fk(start_joints),
                'times': torch.zeros((1,)),
                'ros_traj': ros_traj,
                'ros_plan': plan,
            }
        return traj, info

    def display_trajectory(self, plan):
        display_trajmsg = DisplayTrajectory()
        display_trajmsg.trajectory_start = self.robot.get_current_state()
        display_trajmsg.trajectory.append(plan)
        # Publish
        ros_util.publish_msg(display_trajmsg, self.display_trajectory_publisher, 1, 0.05)


def main():
    np.set_printoptions(precision=3, suppress=True)
    rospy.init_node('generate_trajectory_node', anonymous=True)

    from ll4ma_motion_optimization.envs.collision_object import CollisionObject
    from ll4ma_motion_optimization.envs.collision_environment import \
        CollisionEnvironment
    from ll4ma_motion_optimization.optimizers.trajectory_optimizer import TrajectoryOptimizer

    trajopt = TrajectoryOptimizer(
        solver='preweight_L-BFGS-B',
        timesteps=15,
        max_iterations=100,
        position_weight=10,
        use_collision=True,
        object_frame=False,
        pose_factor=0,
        velocity_factor=0.3,
        ds=1/60,
    )
    colenv = CollisionEnvironment()
    obj_name = 'table'
    colenv.add_object(
        obj_name,
        CollisionObject(
            obj_name,
            'box',
            np.array([0.65, 0.0, 0.307, 0, 0, 0, 1]),
            np.array([0.61, 0.91, 0.6025]),
            use_voxgrid=False,
        )
    )
    obj_name = 'stand'
    pose = np.array([
        [1, 0, 0,  0.71],
        [0, 1, 0,  0.3],
        [0, 0, 1,  0.631],
        [0, 0, 0,  1.0],
    ])
    colenv.add_object(
        obj_name,
        CollisionObject(
            obj_name,
            'box',
            pose,
            np.array([0.07, 0.07, 0.07]),
            color=[0, 0, 1, 1],
            use_voxgrid=False,
        )
    )
    obj_name = 'cleaner'
    pose = np.array([
        [1, 0, 0,  0.73],
        [0, 1, 0, -0.3],
        [0, 0, 1,  0.73],
        [0, 0, 0,  1.0],
    ])
    colenv.add_object(
        obj_name,
        CollisionObject(
            obj_name,
            'box',
            pose,
            np.array([0.12, 0.08, 0.2]),
            use_voxgrid=False,
        )
    )
    trajopt.set_collision_env(colenv)
    trajopt.collision_env.display_objects()

    planner = MoveitPlanner()
    sj = np.array([0., 0.6, 0., -0.8, 0., 1., 0.])
    # pose = np.array([
    #     [ 0, -1,  0,  0.65],
    #     [ 0,  0,  1, -0.25],
    #     [-1,  0,  0,  0.85],
    #     [ 0,  0,  0,  1.00],
    # ])
    pose = np.array([
        [ 1,  0,  0,  0.61],
        [ 0,  0,  1, -0.25],
        [ 0, -1,  0,  0.81],
        [ 0,  0,  0,  1.00],
    ])
    pose2 = pose.copy()
    pose2[1, 3] *= -1
    # pose = np.array([
    #     [ 1,  0,  0,  0.65],
    #     [ 0,  1,  0,  0.30],
    #     [ 0,  0,  1,  0.85],
    #     [ 0,  0,  0,  1.00],
    # ])
    print(pose)
    print(torch_util.transform2quatpose(pose))
    print(trajopt.fk(sj))
    # planner.add_collision_env(colenv.get_planning_scene())
    planner.add_collision_env(colenv.get_moveit_env())
    traj, info = planner.get_trajectory(pose, start_joints=sj)
    traj = info['ros_plan']
    last_joints = sj
    if len(traj.joint_trajectory.points) < 1:
        last_joints = traj.joint_trajectory.points[-1].positions
    end_pose = planner.fk(last_joints)
    print(end_pose)
    print(trajopt.fk(np.array(last_joints)[:7]))
    traj, info = planner.get_trajectory(pose2, start_joints=last_joints, level_traj=False)
    traj = info['ros_plan']
    last_joints = sj
    if len(traj.joint_trajectory.points) < 1:
        last_joints = traj.joint_trajectory.points[-1].positions
    end_pose = planner.fk(last_joints)
    print(end_pose)
    print(trajopt.fk(np.array(last_joints)[:7]))

    moveit_commander.roscpp_shutdown()


if __name__ == '__main__':
    main()
