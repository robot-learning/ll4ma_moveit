# moveit_interface

* Run the bimanual iiwa
    ```
    roslaunch moveit_interface bimanual_iiwa_moveit_interface_service.launch \
    end_effector_right:="robotiq"
    ```
To use a right reflex hand, change the flag as `end_effector_right:="reflex"`