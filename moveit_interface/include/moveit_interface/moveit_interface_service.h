#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit_interface/GetPlan.h>
#include <moveit_interface/GetJacobianPInvPlan.h>
#include <moveit_interface/GetIK.h>
#include <moveit_interface/GetFK.h>
#include <moveit_interface/GetJacobian.h>
#include <moveit_interface/GetJointInfo.h>

#include <sensor_msgs/JointState.h>


namespace moveit_interface
{
  class MoveItInterfaceService
  {
  private:
    ros::NodeHandle nh_;
    ros::ServiceServer get_plan_srv_;
    ros::ServiceServer get_jac_pinv_plan_srv_;
    ros::ServiceServer update_env_srv_;
    ros::ServiceServer get_ik_srv_;
    ros::ServiceServer get_fk_srv_;
    ros::ServiceServer get_jacobian_srv_;
    ros::ServiceServer get_joint_info_srv_;

    // TODO should sort out all these ptrs, I think shared_ptr is the way to go for
    // all of them but the ones that aren't get initialized from output of a
    // function call and I couldn't figure out how to do it with shared_ptr
    moveit::planning_interface::MoveGroupInterface::Plan plan_;
    moveit::planning_interface::PlanningSceneInterface planning_scene_;
    std::shared_ptr<moveit::planning_interface::MoveGroupInterface> move_group_;
    std::shared_ptr<robot_model_loader::RobotModelLoader> robot_model_loader_;
    moveit::core::RobotModelPtr robot_model_;
    std::shared_ptr<moveit::core::RobotState> robot_state_;
    moveit::core::JointModelGroup* joint_group_;

    bool getPlan(moveit_interface::GetPlan::Request& req,
                 moveit_interface::GetPlan::Response& resp);
    bool getJacobianPInvPlan(moveit_interface::GetJacobianPInvPlan::Request& req,
                             moveit_interface::GetJacobianPInvPlan::Response& resp);
    bool getIK(moveit_interface::GetIK::Request& req,
               moveit_interface::GetIK::Response& resp);
    bool getFK(moveit_interface::GetFK::Request& req,
               moveit_interface::GetFK::Response& resp);
    void getJacobian(const sensor_msgs::JointState joint_state,
                     const std::string link_name,
                     Eigen::MatrixXd& jacobian);
    bool getJacobian(moveit_interface::GetJacobian::Request& req,
               moveit_interface::GetJacobian::Response& resp);
    bool getJointInfo(moveit_interface::GetJointInfo::Request& req,
               moveit_interface::GetJointInfo::Response& resp);
    bool updateEnvironment(moveit_interface::GetPlan::Request& req,
                           moveit_interface::GetPlan::Response& resp);
    void getPseudoInverse(const Eigen::MatrixXd &m,
                          Eigen::MatrixXd &m_pinv,
                          const double tolerance);

  public:
    MoveItInterfaceService(const ros::NodeHandle& nh);
    bool init();
  };
}
