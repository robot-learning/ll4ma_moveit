#!/usr/bin/env python
from __future__ import print_function

import rospy
from moveit_interface.srv import GetPlan, GetPlanRequest
from geometry_msgs.msg import Pose, PoseStamped
from sensor_msgs.msg import JointState
from std_msgs.msg import ColorRGBA

import roslib.packages as rp
import sys
sys.path.append(rp.get_pkg_dir("moveit_interface") + "/src/moveit_interface/")
from util import get_plan


if __name__ == '__main__':
    rospy.init_node('test_moveit_plan_request')

    # test trajectory planning to a config

    # from this joint state:
    joint_state = JointState()
    #arm_joints = ["lbr4_j0", "lbr4_j1", "lbr4_j2", "lbr4_j3", "lbr4_j4", "lbr4_j5", "lbr4_j6"]
    arm_joints = ["iiwa_link_" + str(i) for i in range(1,8)] 
    hand_joints = ["index_joint_0", "index_joint_1", "index_joint_2", "index_joint_3",
                   "middle_joint_0", "middle_joint_1", "middle_joint_2", "middle_joint_3",
                   "ring_joint_0", "ring_joint_1", "ring_joint_2", "ring_joint_3",
                   "thumb_joint_0", "thumb_joint_1", "thumb_joint_2", "thumb_joint_3"]

    joint_state.name = arm_joints + hand_joints 

    joint_state.position = [0.1 for i in range(16+len(arm_joints))]

    # to this joint position:
    #target_position = [0.5 for i in range(16+7)]
    target_position = [0.5 for i in range(len(arm_joints))]

    # test API to generate trajectory in collision-free environment

    resp = get_plan(None, target_position, joint_state, "palm_link")
    if resp.success:
        rospy.loginfo("Plan to the goal pose found.")
    else:
        rospy.logerr("Plan to the goal pose found.")
    '''
    raw_input()

    # test trajectory planning to a pose

    pose = Pose()
    pose.position.x=0
    pose.position.y=-0.9
    pose.position.z=0.7
    pose.orientation.x = 0.519
    pose.orientation.y = 0.689
    pose.orientation.z = -0.147
    pose.orientation.w = 0.483
    pose_stamped = PoseStamped()
    pose_stamped.header.frame_id = "world"
    pose_stamped.pose = pose

    # from this joint state:
    joint_state = JointState()
    arm_joints = ["lbr4_j0", "lbr4_j1", "lbr4_j2", "lbr4_j3", "lbr4_j4", "lbr4_j5", "lbr4_j6"]
    hand_joints = ["index_joint_0", "index_joint_1", "index_joint_2", "index_joint_3",
                   "middle_joint_0", "middle_joint_1", "middle_joint_2", "middle_joint_3",
                   "ring_joint_0", "ring_joint_1", "ring_joint_2", "ring_joint_3",
                   "thumb_joint_0", "thumb_joint_1", "thumb_joint_2", "thumb_joint_3"]

    joint_state.name = arm_joints + hand_joints 

    joint_state.position = [0.0 for i in range(16+7)]
    joint_state.position[-1] = 1.2

    # test API to generate trajectory in collision-free environment

    resp = get_plan(pose_stamped, None, joint_state, "palm_link")
    if resp.success:
        rospy.loginfo("Plan to the goal pose found.")
    else:
        rospy.logerr("Plan to the goal pose found.")

    raw_input("press enter to add a box and find a plan")

    # test scene creation
    rospy.loginfo("Testing scene creation...")
    box_pose = Pose()
    box_pose.position.x = 0.
    box_pose.position.y = -0.8
    box_pose.position.z = 0.295
    box_pose.orientation.x = 0. 
    box_pose.orientation.y = 0. 
    box_pose.orientation.z = 0. 
    box_pose.orientation.w = 1. 
    color = ColorRGBA()
    color.b = 1
    box = {
            "frame_id" : "world",
            "object_type" : "box",
            "extents" : [0.9125,0.61,0.59], #0.59
            "pose" : box_pose,
            "operation": 0, # ADD,
            "color": color
          }

    resp = get_plan(pose_stamped, None, joint_state, "palm_link", objects={"box1": box})
    if resp is not None and resp.success:
        rospy.loginfo("Plan to the goal pose found.")
    else:
        rospy.logerr("Plan to the goal pose not found.")

    raw_input("press enter to test adding an object mesh")

    obj_pose = Pose()
    obj_pose.position.x = 0.
    obj_pose.position.y = -0.4
    obj_pose.position.z = 1.295
    obj_pose.orientation.x = 0. 
    obj_pose.orientation.y = 0. 
    obj_pose.orientation.z = 0. 
    obj_pose.orientation.w = 1. 
    color = ColorRGBA()
    color.b = 1
    obj = {
            "frame_id" : "world",
            "object_type" : "mesh",
            "path": "/mnt/tars_data/sim_dataset/BigBird/BigBird_exp_mesh/part0/coffee_mate_french_vanilla/meshes/poisson.ply",
            "pose" : obj_pose,
            "color": color,
            "operation": 0
          }

    resp = get_plan(pose_stamped, None, joint_state, "palm_link", objects={"obj1": obj})
    raw_input("the object should be added now")

    raw_input("press enter to test adding an object mesh")

    obj_pose = Pose()
    obj_pose.position.x = 0.
    obj_pose.position.y = -0.8
    obj_pose.position.z = 1.295
    obj_pose.orientation.x = 0. 
    obj_pose.orientation.y = 0. 
    obj_pose.orientation.z = 0. 
    obj_pose.orientation.w = 1. 
    color = ColorRGBA()
    color.b = 1
    obj = {
            "frame_id" : "world",
            "object_type" : "mesh",
            "path": "/mnt/tars_data/sim_dataset/BigBird/BigBird_exp_mesh/part0/coffee_mate_french_vanilla/meshes/poisson.ply",
            "pose" : obj_pose,
            "color": color,
            "operation": 0
          }

    resp = get_plan(pose_stamped, None, joint_state, "palm_link", objects={"obj2": obj})
    raw_input("another object should be added now")


    obj_pose = Pose()
    obj_pose.position.x = 0.
    obj_pose.position.y = -0.8
    obj_pose.position.z = 1.295
    obj_pose.orientation.x = 0. 
    obj_pose.orientation.y = 0. 
    obj_pose.orientation.z = 0. 
    obj_pose.orientation.w = 1. 
    color = ColorRGBA()
    color.b = 1
    obj = {
            "frame_id" : "world",
            "object_type" : "mesh",
            "path": "/mnt/tars_data/sim_dataset/BigBird/BigBird_exp_mesh/part1/pringles_bbq/meshes/poisson.ply",
            "pose" : obj_pose,
            "color": color,
            "operation": 0
          }

    resp = get_plan(pose_stamped, None, joint_state, "palm_link", objects={"obj2": obj})
    raw_input("an object should be replaced now")
    '''
