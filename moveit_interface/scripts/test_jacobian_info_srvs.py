#!/usr/bin/env python

import torch

from ll4ma_util import ros_util

from moveit_interface.util import get_fk, get_ik, get_jacobian, get_joint_info

def main():
    """
    This function tests the getter functions from the moveit_interface_service
    and gives an example of how to call them.
    - get_fk
    - get_ik
    - get_jacobian
    - get_joint_info
    """

    joint_info, success = get_joint_info()
    urdf_path = joint_info.urdf
    link_names = joint_info.joint_names
    joint_names = [j.replace('link', 'joint') for j in link_names]
    num_joints = len(joint_names)

    limits_lower = torch.tensor(joint_info.lower_position)[:num_joints]
    limits_upper = torch.tensor(joint_info.upper_position)[:num_joints]
    upper_velocity = torch.tensor(joint_info.upper_velocity)[:num_joints]
    lower_velocity = torch.tensor(joint_info.lower_velocity)[:num_joints]

    joint_range = limits_upper - limits_lower
    joint_values = torch.rand((1,num_joints)) * joint_range + limits_lower

    fkresp, success = get_fk(joint_values, joint_names, link_names[-1])
    transform = torch.tensor(ros_util.pose_to_homogeneous(fkresp.poses[0]))

    ikresp, success = get_ik(fkresp.poses, link_names[-1])
    ik_joints = torch.tensor([
      js.position for idx, js in enumerate(ikresp.solutions)
        if ikresp.solution_found[idx]
    ])

    jacresp, success = get_jacobian(joint_values, joint_names, link_names[-1])
    jacobian = torch.stack( [
      torch.tensor(jac.data).reshape(6,7) for jac in jacresp.jacobian
    ] ).squeeze()

    print('joint values:\n', joint_values)
    print('fk pose:\n', transform)
    print('ik joint values:\n', ik_joints)
    print('jacobian:\n', jacobian)


if __name__ == '__main__':
    main()
