#!/usr/bin/env python
import argparse
import numpy as np
import time


from moveit_interface.util import get_fk, get_ik


if __name__ == '__main__':
    """
    This script tests the IK and FK services, generates a 
    bunch of random joint values, makes an FK request, then 
    an IK request on that response, and checks they're similar.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--n_pts', type=int, default=1000)
    args = parser.parse_args()

    link_name = "iiwa_link_7"

    min_jnts = np.array([-2.9, -2.0, -2.9, -2.0, -2.9, -2.0, -3.0])
    max_jnts = np.array([ 2.9,  2.0,  2.9,  2.0,  2.9,  2.0,  3.0])
    joint_names = ["iiwa_joint_" + str(i) for i in range(1,8)] 
    
    joints = []
    for _ in range(args.n_pts):
        js = np.random.randn(7)
        js = np.clip(js, min_jnts, max_jnts)
        joints.append(js.tolist())

    start = time.time()
    fk_resp, _ = get_fk(joints, joint_names, link_name)
    print(f"FK time ({args.n_pts} points): {time.time() - start:.6f}")

    start = time.time()
    ik_resp, _ = get_ik(fk_resp.poses, link_name)
    print(f"IK time ({args.n_pts} points): {time.time() - start:.6f}")

    found_jnts = [js.position for idx, js in enumerate(ik_resp.solutions)
                  if ik_resp.solution_found[idx]]


    found_fk_resp, _ = get_fk(found_jnts, joint_names, link_name)

    orig_poses = [p for idx, p in enumerate(fk_resp.poses) if ik_resp.solution_found[idx]]
    soln_poses = found_fk_resp.poses
    
    print(f"\nSolutions found: {len(soln_poses)} / {args.n_pts}\n")

    close = []
    for orig, soln in zip(orig_poses, soln_poses):
        p1 = np.array([orig.position.x,
                       orig.position.y,
                       orig.position.z,
                       orig.orientation.x,
                       orig.orientation.y,
                       orig.orientation.z,
                       orig.orientation.w])
        p2 = np.array([soln.position.x,
                       soln.position.y,
                       soln.position.z,
                       soln.orientation.x,
                       soln.orientation.y,
                       soln.orientation.z,
                       soln.orientation.w])
        close.append(np.allclose(p1, p2, atol=1e-4))
    print("All close?", all(close))
    
