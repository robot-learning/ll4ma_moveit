#!/usr/bin/env python
import sys
import rospy
import numpy as np
import os.path as osp

from geometry_msgs.msg import PoseStamped
from sensor_msgs.msg import JointState

from moveit_interface.util import GET_JAC_PINV_PLAN_SRV, get_jac_pinv_plan

from ll4ma_util import file_util, ros_util


if __name__ == '__main__':
    """
    This script tests straight-line plans generated from the Jacobian pseudoinverse
    planner. Note you can just run the following to visualize this script:

        roslaunch moveit_interface test_jac_pinv_plan.launch

    You should see rviz come up and it will cycle between different arm trajectories
    of the robot doing straight-line pushes.
    """
    rospy.init_node('test_jac_pinv_plan')

    # I pulled these from some push data I had on my machine
    start_joints = [
        [ 1.842258, 1.463416, 1.869743, 1.804935,-0.44353 ,-0.689377, 0.509845],
        [ 1.029318, 1.028929,-0.702332,-1.627306, 2.17452 ,-1.303346, 0.784061],
        [-1.536534,-1.400856, 1.9364  ,-1.581077,-0.285874, 1.296324,-2.759669],
        [-1.289107, 1.553037, 1.373977,-1.108302, 0.122138, 1.458663,-0.164729],
        [-0.967633, 1.614519, 1.391555,-0.495255, 0.097647, 1.884032,-0.125282]
    ]
    
    start_poses = [
        [ 0.438496, 0.30962 , 0.881023,-0.679833, 0.197043, 0.196984,-0.678379],
        [ 0.536014, 0.095901, 0.903261,-0.57293 , 0.41412 , 0.415784,-0.572171],
        [ 0.429785, 0.253273, 0.889585, 0.587166,-0.397102,-0.394546, 0.584705],
        [ 0.571462,-0.283698, 0.900262,-0.570898,-0.419608,-0.41697 ,-0.569334],
        [ 0.629677,-0.325428, 0.892456,-0.539916,-0.458986,-0.457207,-0.537387]
    ]

    end_poses = [
        [ 0.810355, 0.07061 , 0.881023,-0.679833, 0.197043, 0.196984,-0.678379],
        [ 0.724327,-0.499209, 0.903261,-0.57293 , 0.41412 , 0.415784,-0.572171],
        [ 0.704324,-0.426437, 0.889585, 0.587166,-0.397102,-0.394546, 0.584705],
        [ 0.717664, 0.165107, 0.900262,-0.570898,-0.419608,-0.41697 ,-0.569334],
        [ 0.694589, 0.077441, 0.892456,-0.539916,-0.458986,-0.457207,-0.537387]
    ]

    start_pose_pub = rospy.Publisher("/start_ee_pose", PoseStamped, queue_size=1)
    end_pose_pub = rospy.Publisher("/end_ee_pose", PoseStamped, queue_size=1)
    rate = rospy.Rate(10)
    n_steps = 30

    rospy.loginfo("Waiting for planning service...")
    rospy.wait_for_service(GET_JAC_PINV_PLAN_SRV, 10)
    rospy.loginfo("Planning service found")

    rospy.loginfo("Visualizing trajectories (in rviz)...")
    start_pose_stmp = PoseStamped()
    start_pose_stmp.header.frame_id = "world"
    end_pose_stmp = PoseStamped()
    end_pose_stmp.header.frame_id = "world"
    while not rospy.is_shutdown():
        for joints, start_ee, end_ee in zip(start_joints, start_poses, end_poses):
            start_pose = ros_util.array_to_pose(start_ee)
            end_pose = ros_util.array_to_pose(end_ee)
            start_joint_state = ros_util.get_joint_state_msg(
                joints,
                joint_names=[f"iiwa_joint_{i}" for i in range(1, 8)]
            )
            resp, _ = get_jac_pinv_plan(
                start_joint_state,
                start_pose,
                end_pose,
                'reflex_palm_link',
                velocity=0.5,
                dt=0.1
            )
                        
            ros_util.display_rviz_trajectory(
                resp.trajectory.joint_trajectory,
                JointState(
                    position=joints,
                    name=[f"iiwa_joint_{i}" for i in range(1, 8)]
                ),
                'iiwa'
            )
            
            i = 0
            while not rospy.is_shutdown() and i < n_steps:
                i += 1
                start_pose_stmp.header.stamp = rospy.Time.now()
                start_pose_stmp.pose = start_pose
                start_pose_pub.publish(start_pose_stmp)
                end_pose_stmp.header.stamp = rospy.Time.now()
                end_pose_stmp.pose = end_pose
                end_pose_pub.publish(end_pose_stmp)
                rate.sleep()
