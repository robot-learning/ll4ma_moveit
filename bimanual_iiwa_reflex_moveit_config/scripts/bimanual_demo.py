#!/usr/bin/env python

import rospy
import multiprocessing as mp
from ll4ma_pick_n_place.planning_client import moveit_client
from gpu_sd_map.ros_transforms_lib import convert_array_to_pose

HOME = [0.0] * 7

TOUCH = [0.0, -0.175, 0.0, 0.0, 0.0, 0.0]

LIFT = [0.0, 0.0, 0.5, 0.0, 0.0, 0.0]

PLACE = [-lift_axis for lift_axis in LIFT]

RETRACT = [-element*.8 for element in TOUCH]


TEST_L = [-1.5923359937082024, 0.824226452870184, 0.02988907639975439, -1.9880629497150224, 0.0011173131868357415, -1.2632431257824819, -1.4167168249969624]
TEST_R = TEST_L.copy()
TEST_R[-1] = -TEST_R[-1]
#TEST[1] = -0.5
#TEST[3] = -1


def call_exec(tup_arg):
    plan, ns = tup_arg
    #rospy.init_node(ns+'_plan_exec_node')
    mvcli = moveit_client(ns=ns)
    mvcli.execute_plan(plan)

if __name__ == '__main__':
    rospy.init_node('bimanual_demo_node', anonymous=True)

    planner = moveit_client()

    left_ee_frame = 'left_reflex_palm_link'
    right_ee_frame = 'right_reflex_palm_link'

    
    left_ee_pose = [0.2, 0.2, 1.5, 0.0, 0.0, -1.57]
    left_ee_pose = convert_array_to_pose(left_ee_pose, 'world').pose
    left_joints = planner.get_IK(left_ee_pose, left_ee_frame)

    right_ee_pose = [0.2, -0.2, 1.5, 0.0, 0.0, 1.57]
    right_ee_pose = convert_array_to_pose(right_ee_pose, 'world').pose
    right_joints = planner.get_IK(right_ee_pose, right_ee_frame)

    traj = planner.plan_to_joint(left_joints + right_joints)

    planner.execute_plan(traj)


    left_ee_pose = [-0.3, -0.35, 0.80, 0.0, 0.707, -1.57]
    left_ee_pose = convert_array_to_pose(left_ee_pose, 'world').pose
    left_joints = planner.get_IK(left_ee_pose, left_ee_frame)
    
    traj = planner.plan_to_joint(left_joints + right_joints)

    planner.execute_plan(traj)

    home_traj = planner.plan_to_joint([0.0] * 14)

    planner.execute_plan(home_traj)

    # mvcli_left = moveit_client(ns='left')
    # mvcli_right = moveit_client(ns='right')

    # pool = mp.Pool(processes=2)

    # plan_left = mvcli_left.plan_to_joint(TEST_L)
    # mvcli_left.display_plan(plan_left.joint_trajectory)

    # uip = input('Preshape Left?')
    # if uip.lower() == 'y':
    #     mvcli_left.execute_plan(plan_left)

    # plan_right = mvcli_right.plan_to_joint(TEST_R)
    # mvcli_right.display_plan(plan_right.joint_trajectory)

    # uip = input('Pregrasp Right?')
    # if uip.lower() == 'y':
    #     mvcli_right.execute_plan(plan_right)

    # #args = [(plan_left, mvcli_leftecute_plan), \
    # #        (plan_right, mvcli_right.execute_plan)]

    # args = [(plan_left, 'left'), \
    #         (plan_right, 'right')]

    # #pool.map(call_exec, args)

    # plan_left = mvcli_left.cart_plan(TOUCH)
    # mvcli_left.display_plan(plan_left.joint_trajectory)

    # plan_right = mvcli_right.cart_plan(TOUCH)
    # mvcli_right.display_plan(plan_right.joint_trajectory)

    # args = [(plan_left, 'left'), \
    #         (plan_right, 'right')]
    
    # uip = input('Touch?')
    # if uip.lower() == 'y':
    #     #mvcli_left.execute_plan(plan_left)
    #     pool.map(call_exec, args)


    # plan_left = mvcli_left.cart_plan(LIFT)
    # mvcli_left.display_plan(plan_left.joint_trajectory)

    # plan_right = mvcli_right.cart_plan(LIFT)
    # mvcli_right.display_plan(plan_right.joint_trajectory)

    # args = [(plan_left, 'left'), \
    #         (plan_right, 'right')]
    
    # uip = input('Lift?')
    # if uip.lower() == 'y':
    #     #mvcli_left.execute_plan(plan_left)
    #     pool.map(call_exec, args)


    # plan_left = mvcli_left.cart_plan(PLACE)
    # mvcli_left.display_plan(plan_left.joint_trajectory)

    # plan_right = mvcli_right.cart_plan(PLACE)
    # mvcli_right.display_plan(plan_right.joint_trajectory)

    # args = [(plan_left, 'left'), \
    #         (plan_right, 'right')]
    
    # uip = input('Place?')
    # if uip.lower() == 'y':
    #     #mvcli_left.execute_plan(plan_left)
    #     pool.map(call_exec, args)

    # plan_left = mvcli_left.cart_plan(RETRACT)
    # mvcli_left.display_plan(plan_left.joint_trajectory)

    # plan_right = mvcli_right.cart_plan(RETRACT)
    # mvcli_right.display_plan(plan_right.joint_trajectory)

    # args = [(plan_left, 'left'), \
    #         (plan_right, 'right')]
    
    # uip = input('Retract?')
    # if uip.lower() == 'y':
    #     #mvcli_left.execute_plan(plan_left)
    #     pool.map(call_exec, args)

    # plan_left = mvcli_left.plan_to_joint(HOME)
    # mvcli_left.display_plan(plan_left.joint_trajectory)

    # uip = input('Home Left?')
    # if uip.lower() == 'y':
    #     mvcli_left.execute_plan(plan_left)

    # plan_right = mvcli_right.plan_to_joint(HOME)
    # mvcli_right.display_plan(plan_right.joint_trajectory)

    # uip = input('Home Right?')
    # if uip.lower() == 'y':
    #     mvcli_right.execute_plan(plan_right)

    # args = [(plan_left, 'left'), \
    #         (plan_right, 'right')]

    # #pool.map(call_exec, args)
