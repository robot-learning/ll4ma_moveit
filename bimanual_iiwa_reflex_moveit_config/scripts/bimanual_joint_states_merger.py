#!/usr/bin/env python

import rospy
from sensor_msgs.msg import JointState

class IIWA_Reflex_State:

    def __init__(self):
        self.robot_name = 'iiwa'
        ns = rospy.get_namespace().strip('/')
        self.name_prefix = ""
        if ns != "":
            self.name_prefix = ns + "_"
        self.hand_name = rospy.get_param(f"{self.name_prefix}joint_state_merger/hand_name")
        self.joint_states_dict = {self.robot_name:None, self.hand_name:None}
        self.full_js = None
        self.pub_full_js = rospy.Publisher("joint_states", JointState, queue_size=1)
        for aname in self.joint_states_dict:
            rospy.Subscriber("{}/joint_states".format(aname), JointState, \
                             self._joint_state_cb, aname)

    def _joint_state_cb(self, joint_state, robot_key):
        self.joint_states_dict[robot_key] = joint_state

    def joint_states_merger(self):
        if None not in self.joint_states_dict.values():
            new_js = JointState()
            times = []
            for js in self.joint_states_dict.values():
                times.append(js.header.stamp)
                new_js.name +=  [self.name_prefix + name for name in js.name]
                new_js.position += js.position
                new_js.velocity += js.velocity
                new_js.effort += js.effort
            new_js.header.stamp = min(times)
            return new_js
        return None

    def publish_merged_states(self):
        self.full_js = self.joint_states_merger()
        if self.full_js is not None:
            self.pub_full_js.publish(self.full_js)


if __name__ == '__main__':
    ns = rospy.get_namespace()
    rospy.init_node('jointstates_merger_node', anonymous=False)
    js_manager = IIWA_Reflex_State()
    rate = rospy.Rate(100)
    while not rospy.is_shutdown():
        js_manager.publish_merged_states()
        rate.sleep()
    rospy.spin()
            
