# LL4MA MoveIt

Resources for planning with MoveIt. Offers some classes and services that enable easily making requests to get plans from MoveIt. We also aggregate MoveIt configs here so we don't have separate repos for all of them.

**TODO:** Adam needs to document this stuff better, ask him nicely :)

## Installation
You can put this in a catkin workspace and build as usual. You will at least need to have MoveIt installed, and you will need these internal dependencies:

```
git clone git@bitbucket.org:robot-learning/ll4ma_robots_description.git
git clone git@bitbucket.org:robot-learning/ll4ma_util.git
```